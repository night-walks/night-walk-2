# Night Walk 2 Credits

Most imagery is taken from my original photography. Some silhouettes were drawn from the following:

https://commons.m.wikimedia.org/wiki/File:Bouquetin5.jpg

https://lt.wikipedia.org/wiki/Adiantas#/media/Vaizdas:Adiantum_pedatum_09905.JPG

https://en.wikipedia.org/wiki/Adenophorus_periens#/media/File:Adenophorus_periens_(11637153345).jpg

Carribean monk seal nasal mite https://www.thoughtco.com/recently-extinct-insects-and-invertebrates-1093353

https://en.m.wikipedia.org/wiki/Levuana_moth#/media/File%3ALevuana_iridescens01a.jpg

https://www.earthwormwatch.org/blogs/endangered-earthworms
Giant Palouse Earthworm _Driloleirus americanus_ Credit: Chris Baugher CC BY 2.0 via Wikimedia Commons

https://en.m.wikipedia.org/wiki/Rocky_Mountain_locust#/media/File%3AMelanoplus_spretusAnnReportAgExpStaUM1902B.jpg

https://en.m.wikipedia.org/wiki/Urania_sloanus#/media/File%3AZoological_Illustrations_Volume_III_Series_2_129.jpg

https://en.m.wikipedia.org/wiki/Wahlenbergia_roxburghii#/media/File%3AMELLISS(1875)_p427_-_PLATE_49_-_Wahlenbergia_Burchellii.jpg

https://en.m.wikipedia.org/wiki/Delissea_subcordata#/media/File%3ADelissea_subcordata_(6218757027).jpg

https://vi.m.wikipedia.org/wiki/Galipea#/media/T%E1%BA%ADp_tin%3AGalipea_sp.%2C_Rutaceae%2C_Atlantic_forest%2C_northern_littoral_of_Bahia%2C_Brazil_(12909729905).jpg

https://en.m.wikipedia.org/wiki/Viola_hispida#/media/File%3AViola_hispida.jpg

https://en.m.wikipedia.org/wiki/Flora_of_St_Helena#/media/File%3ACabbage_trees.jpeg

https://en.m.wikipedia.org/wiki/Hibiscadelphus_wilderianus#/media/File%3AHibiscadelphus_Wilderianos.jpg

https://en.m.wikipedia.org/wiki/Marshallia_grandiflora#/media/File%3AMagr7_001_pvd.png

https://commons.m.wikimedia.org/wiki/File:Ochrosia_fatuhivensis.jpg#mw-jump-to-license

