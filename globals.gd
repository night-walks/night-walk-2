extends Node

var connecting_to_server : bool = true;

var specific_dream_on_layer = {
	"neg_4": 0,
	"neg_3": 0,
	"neg_2": 0,
	"neg_1": 0,
	"zero": 0,
	"pos_1": 0,
	"pos_2": 0,
	"pos_3": 0,
	"pos_4": 0
}

# These have been removed from the publicly-available source.
var url = "redacted"
var url_retrieve_data: String = url + "redacted"
var url_add_data: String = url + "redacted"

var autoplay: bool = true
