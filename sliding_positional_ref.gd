extends Node2D

var oldPosition: Vector2

func _process(_delta):
	if (position != oldPosition):
		get_tree().call_group("sliding_objects", "_on_update_position", position)
		oldPosition = position


