extends Node

# This might look like it should just be a part of Vignette.gd to make things simpler, but there might be additional complexity added to the way auto-advancing happens. 

var major_changes_min_time: int = 45
var major_changes_max_time: int = 75
var minor_changes_min_time: int = 10
var minor_changes_max_time: int = 30

func _ready():
    if Globals.autoplay:
        restart_major_timer()
        restart_minor_timer()

func _on_Timer_MajorChanges_timeout():
    get_tree().call_group("Auto-advance listeners", "_on_major_changes")
    restart_major_timer()

func _on_Timer_MinorChanges_timeout():
    get_tree().call_group("Auto-advance listeners", "_on_minor_changes")
    restart_minor_timer()

func restart_major_timer():
    var next_time : int = int(rand_range(major_changes_min_time, major_changes_max_time))
    $"./Timer_MajorChanges".wait_time = next_time
    $"./Timer_MajorChanges".start()
    
func restart_minor_timer():
    var next_time : int = int(rand_range(minor_changes_min_time, minor_changes_max_time))
    $"./Timer_MinorChanges".wait_time = next_time
    $"./Timer_MinorChanges".start()