# Night Walk 2 Instructions

Self-playing, but you can move it along by hitting ENTER and SPACE. F will toggle fullscreen. If it looks weird on Windows, try exiting and re-entering fullscreen.

Data is sent to the server based on what the software is showing on screen. No information about you or your computer is sent.
