extends Node2D

export var sliding: bool
export var z_position: int # not sure if using this or Vignette.gd arrays
var is_leaving: bool = false

# Input strings of paths to sub-nodes that also have animation players, if any
export (Array, String) var additional_animation_players

func _ready():
	if sliding:
		add_to_group("sliding_objects")
	visible = true
	if !self.is_in_group("vignette_elements"):
		self.add_to_group("vignette_elements")
	get_z_position()

func get_z_position():
	var myparent: String = self.get_parent().name
	match [myparent]:
		["-5_Sky"]:
			z_position = -5
		["-4_Setting"]:
			z_position = -4
		["-3_Theme"]:
			z_position = -3
		["-2_Connector"]:
			z_position = -2
		["-1_Person_small"]:
			z_position = -1
		["0_Middle_ground"]:
			z_position = 0
		["1_Entity"]:
			z_position = 1
		["2_Foregrounding"]:
			z_position = 2
		["3_Person_large"]:
			z_position = 3
		["4_Foreground"]:
			z_position = 4
	

func _on_update_position(new_pos: Vector2):
	position = new_pos

func _on_leave_scene(var z_position_leaving: int = -1000):
	# print(self.name, " is leaving.")
	# Check if only objects on certain z position self-destroy
	if z_position_leaving != -1000 && z_position != z_position_leaving:
		return
	is_leaving = true
	if (additional_animation_players.size() > 0):
		for anim in additional_animation_players:
			var animnode: AnimationPlayer = get_node(anim)
			animnode.play_backwards()
	if has_node("AnimationPlayer"):
		$AnimationPlayer.play_backwards()
	else:
		print(self.name, " destroyed itself before playing animation")
		self.queue_free()
	
func _on_destroy_self():
	if !is_leaving:
		return
	self.queue_free()
