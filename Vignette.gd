extends Node2D

var currently_displayed_objects = []

var neg_4 = [
	"-4_Setting",
	"res://VignetteImages/Vignette_A/vignette_a_trees.tscn",
	"res://VignetteImages/Vignette_B/vignette_b_trees.tscn",
	"res://VignetteImages/Vignette_C/vignette_c_background_sliding.tscn",
	"res://VignetteImages/Vignette_C/vignette_c_ground_sliding.tscn",
	"res://VignetteImages/Vignette_D/vignette_d_trees.tscn",
	"res://VignetteImages/Vignette_E/vignette_e_background.tscn",
	"res://VignetteImages/Vignette_F/vignette_f_backdrop.tscn",
	"res://VignetteImages/Vignette_G/vignette_g_grass.tscn",
	"res://VignetteImages/landscape_a.tscn",
	"res://VignetteImages/landscape_b.tscn",
	"res://VignetteImages/Vignette_J/vignette_j_mountains.tscn",
	"res://VignetteImages/Vignette_L/vignette_l_water.tscn",
	"res://VignetteImages/Past/past_f_seaweed.tscn",
	"res://VignetteImages/Vignette_M/vignette_m_ground.tscn"
]

var neg_3 = [
	"-3_Theme",
	"res://VignetteImages/Vignette_A/vignette_a_house.tscn",
	"res://VignetteImages/Vignette_C/vignette_c_trailer_sliding.tscn",
	"res://VignetteImages/Vignette_G/vignette_g_bush.tscn",
	"res://VignetteImages/Vignette_L/vignette_l_island.tscn",
	"res://VignetteImages/Past/Past_A/past_a_sigillaria.tscn",
	"res://VignetteImages/Past/past_b.tscn",
	"res://VignetteImages/Past/past_c.tscn",
	"res://VignetteImages/Past/past_d_curlew.tscn",
	"res://VignetteImages/Past/past_e.tscn",
	"res://VignetteImages/Past/past_g_adiantum.tscn",
	"res://VignetteImages/Past/past_h.tscn",
	"res://VignetteImages/Past/Past_Moth/past_moth.tscn",
	"res://VignetteImages/Past/Past_Snail/past_snail.tscn",
	"res://VignetteImages/Past/Past_Locust/past_locust.tscn",
	"res://VignetteImages/Past/past_olive.tscn",
	"res://VignetteImages/Vignette_M/vignette_m_street.tscn",
	"res://VignetteImages/Past/past_bellflower.tscn",
	"res://VignetteImages/Past/past_galipea.tscn"
]

var neg_2 = [
	"-2_Connector",
	"res://VignetteImages/Vignette_A/vignette_a_grass.tscn",
	"res://VignetteImages/Vignette_B/vignette_b_fenceGrass.tscn",
	"res://VignetteImages/Vignette_C/vignette_c_trees_sliding.tscn",
	"res://VignetteImages/Vignette_D/vignette_d_ground.tscn",
	"res://VignetteImages/Vignette_E/vignette_e_grass.tscn",
	"res://VignetteImages/Vignette_F/vignette_f_ground.tscn",
	"res://VignetteImages/Vignette_G/vignette_g_tree.tscn",
	"res://VignetteImages/Vignette_J/vignette_j_connector.tscn",
	"res://VignetteImages/Vignette_L/vignette_l_beach.tscn",
	"res://VignetteImages/Past/past_j_adiantas.tscn",
	"res://VignetteImages/Past/past_k_pendantKihiFern.tscn",
	"res://VignetteImages/Past/Past_Nasal_Mite/past_nasal_mite.tscn"
]

var neg_1 = [
	"-1_Person_small",
	"res://VignetteImages/People/person_small_a.tscn",
	"res://VignetteImages/People/person_small_b.tscn",
	"res://VignetteImages/People/person_small_c.tscn",
	"res://VignetteImages/People/person_small_d.tscn",
	"res://VignetteImages/People/person_small_e.tscn",
	"res://VignetteImages/People/person_small_f.tscn",
	"res://VignetteImages/People/person_small_g.tscn",
	"res://VignetteImages/People/person_small_h.tscn",
	"res://VignetteImages/People/person_small_i.tscn",
	"res://VignetteImages/People/person_small_j.tscn",
	"res://VignetteImages/People/person_small_k.tscn",
	"res://VignetteImages/People/person_small_l.tscn",
	"res://VignetteImages/People/person_small_m.tscn",
	"res://VignetteImages/People/person_small_n.tscn"
]

var zero = [
	"0_Middle_ground",
	"res://VignetteImages/Vignette_B/vignette_b_trashcan.tscn",
	"res://VignetteImages/Vignette_B/vignette_b_house.tscn",
	"res://VignetteImages/Vignette_B/vignette_b_stairs.tscn",
	"res://VignetteImages/Vignette_C/vignette_c_waterTower_sliding.tscn",
	"res://VignetteImages/Vignette_C/vignette_c_cars_sliding.tscn",
	"res://VignetteImages/Vignette_D/vignette_d_signs.tscn",
	"res://VignetteImages/Vignette_E/vignette_e_museum.tscn",
	"res://VignetteImages/Vignette_E/vignette_e_wood.tscn",
	"res://VignetteImages/Vignette_F/vignette_f_houses.tscn",
	"res://VignetteImages/Vignette_G/vignette_g_structures.tscn",
	"res://VignetteImages/Vignette_J/vignette_j_tree.tscn",
	"res://VignetteImages/Vignette_L/vignette_l_cows.tscn",
	"res://VignetteImages/Past/past_grandiflora.tscn"
]

var pos_1 = [
	"1_Entity",
	"res://VignetteImages/Entities/Entity_A/Entity_A.tscn",
	"res://VignetteImages/Entities/Entity_B/Entity_B.tscn",
	"res://VignetteImages/Entities/Entity_C/Entity_C.tscn",
	"res://VignetteImages/Entities/Entity_D/Entity_D.tscn",
	"res://VignetteImages/Entities/Entity_E/Entity_E.tscn",
	"res://VignetteImages/Entities/Entity_F/Entity_F.tscn",
	"res://VignetteImages/Entities/Entity_G/Entity_G.tscn",
	"res://VignetteImages/Entities/Entity_H/Entity_H.tscn",
	"res://VignetteImages/Entities/Entity_I/Entity_I.tscn",
	"res://VignetteImages/Past/Past_ibex/past_ibex.tscn",
	"res://VignetteImages/Past/Past_Earthworm/past_earthworm.tscn",
	"res://VignetteImages/Entities/Entity_J/Entity_J.tscn"
]

var pos_2 = [
	"2_Foregrounding",
	"res://VignetteImages/Vignette_A/vignette_a_fence.tscn",
	"res://VignetteImages/Vignette_C/vignette_c_house_sliding.tscn",
	"res://VignetteImages/People/person_large_f_couch.tscn",
	# "res://VignetteImages/Vignette_D/vignette_d_building.tscn",
	"res://VignetteImages/Vignette_D/vignette_d_table_chair.tscn",
	"res://VignetteImages/Vignette_E/vignette_e_foreground.tscn",
	"res://VignetteImages/Vignette_F/vignette_f_trees.tscn",
	"res://VignetteImages/Vignette_G/vignette_g_cats.tscn",
	"res://VignetteImages/Shapes/foregrounding_shape_a.tscn",
	"res://VignetteImages/Shapes/foregrounding_shape_b.tscn",
	# "res://VignetteImages/Shapes/form_b.tscn",
	"res://VignetteImages/Past/past_i_moss.tscn",
	"res://VignetteImages/Past/Past_Delissea/past_delissea.tscn",
	"res://VignetteImages/Past/past_tire.tscn",
	"res://VignetteImages/Past/past_ochrosia.tscn",
	"res://VignetteImages/Past/past_wilderia.tscn",
	"res://VignetteImages/Past/past_hispida.tscn",
	"res://VignetteImages/Past/past_cabbage.tscn"
]

var pos_3 = [
	"3_Person_large",
	"res://VignetteImages/People/person_large_a.tscn",
	"res://VignetteImages/People/person_large_b.tscn",
	"res://VignetteImages/People/person_large_c.tscn",
	"res://VignetteImages/People/person_large_d_sliding.tscn",
	"res://VignetteImages/People/person_large_e_main.tscn",
	"res://VignetteImages/People/person_large_f.tscn",
	"res://VignetteImages/People/person_large_g.tscn",
	"res://VignetteImages/People/person_large_h.tscn",
	"res://VignetteImages/People/person_large_i.tscn",
	"res://VignetteImages/People/person_large_j.tscn",
	"res://VignetteImages/People/person_large_k.tscn",
	"res://VignetteImages/People/person_large_l.tscn",
	"res://VignetteImages/People/person_large_n.tscn",
	"res://VignetteImages/People/person_large_m.tscn",
	"res://VignetteImages/People/person_large_o.tscn",
	"res://VignetteImages/People/person_large_p.tscn",
	"res://VignetteImages/People/person_large_q.tscn",
	"res://VignetteImages/People/person_large_r.tscn",
	"res://VignetteImages/People/person_large_s.tscn",
	"res://VignetteImages/People/Person_Large_T/person_large_t.tscn"
]

var pos_4 = [
	"4_Foreground",
	"res://VignetteImages/Vignette_A/vignette_a_bush.tscn",
	"res://VignetteImages/Shapes/foreground_shape_a.tscn",
	"res://VignetteImages/Shapes/foreground_shape_b.tscn",
	"res://VignetteImages/Vignette_G/vignette_g_boards.tscn",
	"res://VignetteImages/Shapes/Foreground_Shape_C/foreground_shape_c.tscn",
	"res://VignetteImages/Shapes/foreground_shape_d.tscn",
	"res://VignetteImages/Shapes/Foreground_Shape_E/foreground_shape_e.tscn",
	"res://VignetteImages/Vignette_H/vignette_h_building.tscn",
	"res://VignetteImages/vignette_i.tscn",
	"res://VignetteImages/Shapes/Foreground_Shape_F/foreground_shape_f.tscn",
	"res://VignetteImages/Shapes/foreground_shape_g.tscn",
	"res://VignetteImages/Shapes/foreground_shape_h.tscn",
	"res://VignetteImages/Shapes/foreground_shape_i.tscn",
	"res://VignetteImages/Shapes/Foreground_Shape_J/foreground_shape_j.tscn",
	"res://VignetteImages/Shapes/foreground_shape_l.tscn",
	"res://VignetteImages/Shapes/form_a.tscn",
	"res://VignetteImages/Shapes/foreground_shape_m.tscn",
	"res://VignetteImages/Shapes/Foreground_Shape_N/foreground_shape_n.tscn",
	"res://VignetteImages/Shapes/Foreground_Shape_O/foreground_shape_o.tscn",
	"res://VignetteImages/Vignette_C/vignette_c_bush_sliding.tscn",
	"res://VignetteImages/Shapes/Foreground_Shape_P/foreground_shape_p.tscn",
	"res://VignetteImages/Shapes/foreground_shape_q.tscn",
	"res://VignetteImages/Shapes/foreground_shape_r.tscn",
	"res://VignetteImages/Shapes/Foreground_Shape_S/foreground_shape_s.tscn",
	"res://VignetteImages/Shapes/Foreground_Shape_U/foreground_shape_u.tscn",
	"res://VignetteImages/Shapes/foreground_shape_u2.tscn",
	"res://VignetteImages/Shapes/Foreground_Shape_Water/foreground_shape_water.tscn"
]

var all_lists = [
	neg_4.duplicate(true), neg_3.duplicate(true), neg_2.duplicate(true), 
	neg_1.duplicate(true), zero.duplicate(true), pos_1.duplicate(true), 
	pos_2.duplicate(true), pos_3.duplicate(true), pos_4.duplicate(true)
]

func _ready():
	add_to_group("Auto-advance listeners")
	new_random_scene()

func _process(_delta):
	get_input()

func get_input():
	if Input.is_action_just_released("randomize_scene"):
		new_random_scene()
	if Input.is_action_just_released("swap_element"):
		swap_one_random_scene_element()

func new_random_scene():
	remove_previous_objects()
	randomize()
	for layer in all_lists:
		instantiate_element(layer)

func swap_one_random_scene_element():
	randomize()
	var random_layer: int = int(rand_range(0, all_lists.size()))
	remove_object_from_x_layer(random_layer)
	add_object_to_x_layer(random_layer)

func swap_scene_element_from_layer(layer):
	var layer_num: int = get_layer_num_from_string(layer)
	remove_object_from_x_layer(layer_num)
	add_object_to_x_layer(layer_num)

func remove_object_from_x_layer(var layer: int):
	var neg_layer_offset: int = layer - 4
	get_tree().call_group("vignette_elements", "_on_leave_scene", neg_layer_offset)
	
func add_object_to_x_layer(var layer: int):
	instantiate_element(all_lists[layer])

func remove_previous_objects():
	get_tree().call_group("vignette_elements", "_on_leave_scene")

func instantiate_element(var items):
	var parent = get_node(items[0])
	if items.size() == 1:
		print(parent.name, " has nothing in the array.")
		return
	var random_item: int = int(rand_range(1, items.size())) # skip item 0
	var new_scene = load(items[random_item])
	var new_item = new_scene.instance()
	parent.add_child(new_item)
	currently_displayed_objects.append(new_item)
	remove_item_from_list(random_item, items[0])
	alert_layer_change(random_item, items[0])
	
func remove_item_from_list(var item: int, var list_name: String):
	match list_name:
		"-4_Setting":
			all_lists[0].remove(item)
			if all_lists[0].size() == 1:
				print("refreshing -4")
				all_lists[0] = neg_4.duplicate(true)
		"-3_Theme":
			all_lists[1].remove(item)
			if all_lists[1].size() == 1:
				print("refreshing -3")
				all_lists[1] = neg_3.duplicate(true)
		"-2_Connector":
			all_lists[2].remove(item)
			if all_lists[2].size() == 1:
				print("refreshing -2")
				all_lists[2] = neg_2.duplicate(true)
		"-1_Person_small":
			all_lists[3].remove(item)
			if all_lists[3].size() == 1:
				print("refreshing -1")
				all_lists[3] = neg_1.duplicate(true)
		"0_Middle_ground": 
			all_lists[4].remove(item)
			if all_lists[4].size() == 1:
				print("refreshing zero")
				all_lists[4] = zero.duplicate(true)
		"1_Entity": 
			all_lists[5].remove(item)
			if all_lists[5].size() == 1:
				print("refreshing 1")
				all_lists[5] = pos_1.duplicate(true)
		"2_Foregrounding": 
			all_lists[6].remove(item)
			if all_lists[6].size() == 1:
				print("refreshing 2")
				all_lists[6] = pos_2.duplicate(true)
		"3_Person_large": 
			all_lists[7].remove(item)
			if all_lists[7].size() == 1:
				print("refreshing 3")
				all_lists[7] = pos_3.duplicate(true)
		"4_Foreground": 
			all_lists[8].remove(item)
			if all_lists[8].size() == 1:
				print("refreshing 4")
				all_lists[8] = pos_4.duplicate(true)
		_:
			print("Error: didn't match list")

func _on_major_changes():
	new_random_scene()

func _on_minor_changes():
	swap_one_random_scene_element()

# func _on_change_in_num_changes_to_layer(layer: String):
# 	print ("noticed a change to ", layer)
# 	swap_scene_element_from_layer(layer)

func alert_layer_change(var item: int, var layer: String):
	get_tree().call_group("layer_change", "_on_layer_change", layer, item)
	
	
func get_layer_num_from_string(var layer: String):
	var layer_num: int
	match layer:
		"-4_Setting":
			layer_num = 0
		"-3_Theme":
			layer_num = 1
		"-2_Connector":
			layer_num = 2
		"-1_Person_small":
			layer_num = 3
		"0_Middle_ground": 
			layer_num = 4
		"1_Entity": 
			layer_num = 5
		"2_Foregrounding": 
			layer_num = 6
		"3_Person_large": 
			layer_num = 7
		"4_Foreground": 
			layer_num = 8
		"neg_4":
			layer_num=0
		"neg_3":
			layer_num=1
		"neg_2":
			layer_num=2
		"neg_1":
			layer_num=3
		"zero": 
			layer_num=4
		"pos_1": 
			layer_num=5
		"pos_2": 
			layer_num=6
		"pos_3": 
			layer_num=7
		"pos_4": 
			layer_num=8
		_:
			print("Error: didn't match layer string")
	return layer_num
