# Night Walk 2 v 0.8

## Description
Source code for this project: [link](https://aaronoldenburg.itch.io/night-walk-2)
The public repository is separate from the main, private respository. As this is part of a larger multi-program project that is asynchronously-networked, I removed information on connecting to the specific server. It is still playable without the networked component.


## Installation
It was created in Godot game engine 3.4.4. Builds are provided at the above link.

## Authors and acknowledgment
All work is my own (Aaron Oldenburg) but some images were inspired by photos in the CC and public domain. See the credits.md file.

## License
CC-BY

## Project status
I'm considering this more-or-less finished, but will likely make revisions. As this is separate from the main repository and I have to copy over manually, it might not be as up-to-date as the posted builds.
