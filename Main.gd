extends Node2D

var game_version_on_server: float = 0.0

func _ready(): 
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)

	if Globals.connecting_to_server:
		get_initial_server_data()

func _process(_delta):
	get_input()

func get_input():
	if Input.is_action_just_released("quit"):
		get_tree().quit()
	elif Input.is_action_just_released("fullscreen"):
		OS.window_fullscreen = !OS.window_fullscreen

func get_initial_server_data():
	check_game_version_on_server()

func check_game_version_on_server():
	var error
	error = $Networking/HTTPReq_version_check.request(Globals.url_retrieve_data + "&thought_recalled=version")
	if error != OK:
		push_error("HTTPReq_version_check had an error.")

func _on_HTTPReq_version_request_completed(_result:int, _response_code:int, _headers:PoolStringArray, body:PoolByteArray):
	var text = body.get_string_from_utf8()
	if text == null:
		print ("There is no game data on the server.")
		return
	var myresult: float = float(text)
	var local_version : float = ProjectSettings.get_setting("application/config/version")
	if myresult == local_version:
		return
	if myresult < local_version:
		update_version_number_on_server(local_version)
	elif myresult > local_version:
		print("This is an older version of the game. Disconnecting from server.")
		make_local_only()

func update_version_number_on_server(var version:float):
	var error
	error = $Networking/HTTPReq_version_update.request(Globals.url_add_data + "&thought=version&value='" + String(version) + "'")
	if error != OK:
		push_error("HTTPReq_version_update had an error.")
	
# Turns off future server queries if local version is too old
func make_local_only():
	Globals.connecting_to_server = false

func _on_check_server_timer_timeout():
	check_game_version_on_server()
