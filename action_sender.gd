extends Node

var todo = Array()

func _on_layer_change(var layer: String, var item_num: int):
	var req: String = Globals.url_add_data + "&thought=" + layer + "&quantity='" + String(item_num) +"'"
	upload_data(req)
		
func upload_data(var url: String):
	var error = $"../HTTPReq_layer_change".request(url)
	# If HTTPReq is busy, add to todo queue
	if error == 44:
	   todo.append(url) 
	if error != OK:
		push_error("HTTPReq_layer_change had an error: " + String(error))


func _on_HTTPReq_layer_change_request_completed(_result, _response_code, _headers, _body):
	check_todo_list()

func check_todo_list():
	if todo.size() > 0:
		var next_req = todo.pop_front()
		upload_data(next_req)
