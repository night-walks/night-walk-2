extends AnimatedSprite

export var min_time_swap: float = 5
export var max_time_swap: float = 20
var is_playing_swap_anim: bool = false

func _ready():
	animation = "main"
	reset_timer()
	
func reset_timer():
	randomize()
	$swap_timer.wait_time = rand_range(min_time_swap, max_time_swap)
	$swap_timer.start()


func _on_swap_timer_timeout():
	is_playing_swap_anim = true
	animation = "alternate"


func _on_animation_finished():
	if !is_playing_swap_anim:
		return
	is_playing_swap_anim = false
	animation = "main"
	reset_timer()
